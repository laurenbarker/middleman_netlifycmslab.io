---
title: How we upgraded PostgreSQL at GitLab.com
date: 2020-09-14T10:31:37.358Z
description: We explain the precise maintenance process to execute a major
  version upgrade of PostgreSQL.
image: /images/uploads/cloudnative.jpg
---
We teamed up with [OnGres](https://ongres.com/) to perform a major version upgrade of GitLab.com's main Postgres cluster from version 9.6 to 11 back in May 2020. We upgraded it during a maintenace window, and it all went according to plan. We unpack all that was involved – from planning, testing, and full process automation – to achieve a near-perfect execution of the PostgreSQL upgrade. The full operation was recorded and you can [watch it on GitLab Unfiltered](https://youtu.be/TKODwTtKWew).

The biggest challenge was to do a complete fleet major upgrade through an orchestrated [pg_upgrade](https://www.postgresql.org/docs/11/pgupgrade.html). We needed to have a rollback plan to optimize our capacity right after [Recovery Time Objective (RTO)](https://en.wikipedia.org/wiki/Disaster_recovery) while maintaining a 12-node cluster’s 6TB-data consistent serving 300.000 aggregated transactions per second from around six million users.

![How to edit in Web IDE](/images/uploads/dropdown-choose-your-editor.png "How to edit in Web IDE")

*This is my image caption*

```
gitlab-ci.yml
...more code...
```

## Here is some more text to make a heading

Another section.